package com.company.vapasi.project.Calculator;

public class DivisionOperation implements Operations {
    @Override
    public Object operation ( Object number1 , Object number2 ) {
        if ((number1 instanceof Integer) && (number2 instanceof Integer)) {
            return ((Integer) number1 / (Integer) number2);
        } else if ((number1 instanceof Float) && (number2 instanceof Float)) {
            return ((Float) number1 / (Float) number2);
        } else if ((number1 instanceof Double) && (number2 instanceof Double)) {
            return ((Double) number1 / (Double) number2);
        } else if ((number1 instanceof Long) && (number2 instanceof Long)) {
            return ((Long) number1 / (Long) number2);
        } else {
            return "";
        }
    }
}
