package com.company.vapasi.project.Calculator;

import java.util.Scanner;

public class Calculator {
   static int number1;
    static int number2;
    public static void main ( String[] args ) {
        System.out.println ("Enter first number ->");
        Scanner scan = new Scanner ( System.in );
        number1 = scan.nextInt ();
        System.out.println ("Enter second number ->");
        number2 = scan.nextInt ();
        System.out.println ("Enter the operation to be performed");
        System.out.println (" 1. ADDITION");
        System.out.println (" 2. SUBTRACTION");
        System.out.println (" 3. MULTIPLICATION");
        System.out.println (" 4. DIVISION");
        int operation = scan.nextInt ();
        switch(operation){
            case 1:{
                AdditionOperation ops = new AdditionOperation ();
                System.out.println (ops.operation ( number1, number2));
                break;
            } case 2:{
                SubtractionOperation ops = new SubtractionOperation ();
                System.out.println (ops.operation ( number1, number2));
                break;
            } case 3:{
                MultiplicationOperator ops = new MultiplicationOperator ();
                System.out.println (ops.operation ( number1, number2));
                break;
            } case 4:{
                DivisionOperation ops = new DivisionOperation ();
                System.out.println (ops.operation ( number1, number2));
                break;
            }
        }


    }
}
