package com.company.vapasi.project.ShoppingCart;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class ClothesCatalogueHandler {
    static Map <String, ArrayList <Product>> universalCatalogue;

    public static Map <String, ArrayList <Product>> createCatalogue () {
        universalCatalogue = new HashMap <String, ArrayList <Product>> ();
        return universalCatalogue;
    }

    public void populateData () {
        ClothesCatalogueHandler.createCatalogue ();
        ClothesCatalogueHandler.addSampleContent ();
    }

    public void addProductToMainCatalogue ( String brandName , Product product ) {
        ArrayList <Product> prodList = universalCatalogue.get ( brandName );
        if (prodList != null && !(prodList.isEmpty ())) {
            Product temp = prodList.get(0);
            if(temp.getProductName ().equalsIgnoreCase ( product.getProductName () )){
                prodList.remove ( 0 );
                prodList.add ( product );
            }else{
                prodList.add ( product );
            }
            universalCatalogue.put ( brandName , prodList );
        } else if(prodList.isEmpty ()){
            prodList = new ArrayList <Product> ();
            prodList.add ( product );
            universalCatalogue.put ( brandName , prodList );
        }

    }

    public void printCatalogue ( Map catalogue ) {
        ArrayList <String> brandList = new ArrayList <String> ();
        ArrayList <Product> productArrayList = new ArrayList <Product> ();

        if (catalogue != null) {
            System.out.println ( "**************************CATALOGUE*************************" );
            for (Object entry : catalogue.entrySet ()) {
                if (entry instanceof Map.Entry) {
                    brandList.add ( (((Map.Entry) entry).getKey ()).toString () );
                    productArrayList = ((ArrayList <Product>) (((Map.Entry) entry).getValue ()));
                    printListContentAsTable (  (((Map.Entry) entry).getKey ()).toString ()  ,productArrayList );

                }
            }
            ;
            //System.out.println ( brandList.stream ().collect ( Collectors.toList () ) );

        }
    }

    public static void printListContentAsTable (String brand,ArrayList <Product> productArrayList ) {
        System.out.println ( "~~~~~~~~~~~~~~~~~~~"+brand+"~~~~~~~~~~~~~~~~~~~~~~~" );
        System.out.println ( "Product Name " + "\t"+ "\t"+"Available Quantity");
        for (Product p : productArrayList) {
            System.out.println ( p.getProductName ()+"\t"+ "\t"+"\t"+ "\t"+ p.getAvailableUnit () );
            System.out.println ( "" );


        }

    }

    public static void addSampleContent () {
        ArrayList <Product> zaraContent = new ArrayList <Product> ();
        ArrayList <Product> bibaContent = new ArrayList <Product> ();
        ArrayList <Product> pumaContent = new ArrayList <Product> ();
        ArrayList <Product> levisContent = new ArrayList <Product> ();

        Product zara0 = new Product ( "ZARA" , "CLOTHES" , "SKIRT" , 100 );
        zaraContent.add ( zara0 );
        Product zara1 = new Product ( "ZARA" , "CLOTHES" , "PANT" , 100 );
        zaraContent.add ( zara1 );
        Product zara2 = new Product ( "ZARA" , "CLOTHES" , "TOP" , 100 );
        zaraContent.add ( zara2 );
        Product prod3 = new Product ( "PUMA" , "CLOTHES" , "JEANS" , 100 );
        pumaContent.add ( prod3 );
        Product zara4 = new Product ( "ZARA" , "ACCESSORY" , "BELT" , 60 );
        zaraContent.add ( zara4 );
        Product zara5 = new Product ( "ZARA" , "ACCESSORY" , "PURSE" , 10 );
        zaraContent.add ( zara5 );
        Product puma6 = new Product ( "PUMA" , "ACCESSORY" , "SKIRT" , 100 );
        pumaContent.add ( puma6 );
        Product prod6 = new Product ( "LEVIS" , "CLOTHES" , "JEANS" , 50 );
        levisContent.add ( prod6 );
        Product prod7 = new Product ( "BIBA" , "CLOTHES" , "KURTA" , 50 );
        bibaContent.add ( prod7 );
        Product prod8 = new Product ( "BIBA" , "CLOTHES" , "PALAZZO" , 50 );
        bibaContent.add ( prod8 );
        Product prod9 = new Product ( "BIBA" , "ACCESSORY" , "DUPATTA" , 50 );
        bibaContent.add ( prod9 );
        universalCatalogue.put ( "ZARA" , zaraContent );
        universalCatalogue.put ( "BIBA" , bibaContent );
        universalCatalogue.put ( "LEVIS" , levisContent );
        universalCatalogue.put ( "PUMA" , pumaContent );


    }


}
