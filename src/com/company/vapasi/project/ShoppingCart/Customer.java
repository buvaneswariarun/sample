package com.company.vapasi.project.ShoppingCart;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Customer {
    static Order customerOrder;
    static ArrayList <Product> orderedProducts;
    static ArrayList <Map <String, Product>> universalCatalogue;
    private static ClothesCatalogueHandler clothesCatalogueHandler;

    public static ArrayList <String> printBrandList () {
        ArrayList <String> brandList = new ArrayList <String> ();
        if (ClothesCatalogueHandler.universalCatalogue != null) {
            for (Map.Entry <String, ArrayList <Product>> entry : ClothesCatalogueHandler.universalCatalogue.entrySet ()) {
                System.out.print ( entry.getKey () + "\t" );
                brandList.add ( entry.getKey () );
            }
        }
        return brandList;
    }

    public static void buildCart ( String customerName , ArrayList <Product> orderedProducts ) {

        customerOrder = new Order ( customerName , orderedProducts );
        System.out.printf ( "Order is confirmed. Thanks for shopping with us" );

    }

    public static Product printBrandRelatedItems ( String brandName ) {
        System.out.println ( "Available Stock is listed below. Choose from the same" );
        ArrayList <Product> productList = ClothesCatalogueHandler.universalCatalogue.get ( brandName );
        for (Product prod : productList) {
            System.out.println ( "++++++++++++++++++" + brandName + "++++++++++++++++++++" );
            System.out.println ( " PRODUCT NAME " + "\t" + "\t" + "AVAILABLE QUANTITY" );
            System.out.println ( "\t" + prod.productName + "\t" + "::" + prod.getAvailableUnit () );
        }
        Product selectedProduct = new Product ( brandName );
        System.out.println ( " Please Enter Product you want to order" );
        Scanner scanf = new Scanner ( System.in );
        String selectedClothName = scanf.nextLine ();
        selectedProduct.setProductName ( selectedClothName );
        System.out.println ( " Please enter the quantity you want to order" );
        int quant = scanf.nextInt ();
        selectedProduct.setQuantityOrdered ( quant );
        return selectedProduct;
    }

    public static void viewOrder ( Order order ) {
        if (order != null) {
            for (Product orderedProduct : order.getOrderedItems ()) {
                System.out.println ( "" );
                System.out.println ( "CART DETAILS" );
                System.out.println ( "Your order is " );
                System.out.println ( "CustomerName == " + order.customerName );
                System.out.print ( orderedProduct.getBrandName ()+"\t"+"\t" );
                System.out.print ( orderedProduct.getProductName ()+"\t"+"\t"  );
                System.out.print ( orderedProduct.getQuantityOrdered () );
            }
        }
    }

    public static void mainMenu( String customername ) {

        ArrayList <String> brandList = new ArrayList <String> ();
        System.out.println ( "Please choose products from the brands given below" );
        brandList = printBrandList ();
        System.out.println ();

        Scanner scan = new Scanner ( System.in );
        String answer = scan.nextLine ();

        if (brandList.contains ( answer )) {
            Product selectedProduct = printBrandRelatedItems ( answer );
            System.out.println ( "1. Confirm Order" );
            System.out.println ( "2. Order Again" );
            Scanner scanf = new Scanner(System.in);
            switch (scanf.nextInt ()) {
                case 1: {
                    orderedProducts = new ArrayList <Product> (  );
                    orderedProducts.add ( selectedProduct );
                    buildCart ( customername , orderedProducts );
                    viewOrder ( customerOrder );
                    System.exit ( 0 );
                }
                case 2: {
                    mainMenu(customername);
                }

            }

        }


    }

    public static void main ( String[] args ) {
        clothesCatalogueHandler = new ClothesCatalogueHandler ();
        clothesCatalogueHandler.populateData ();
        System.out.println ("WELCOME TO ONLINE SHOPPING EXPERIENCE");
        System.out.println ( "Dear Customer - Enter your name ::" );
        Scanner namescan = new Scanner ( System.in );
        String customername = namescan.nextLine ();
        mainMenu ( customername );



    }
}
