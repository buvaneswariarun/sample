package com.company.vapasi.project.ShoppingCart;

public class Product {

    String brandName;
    String productType;
    String productName;
    int quantityOrdered;
    int availableUnit;
    String accName;
    int accAvlb;

    public Product ( String brandName ) {
        this.brandName = brandName;
    }

    public Product ( String brandName , String productType , String productName , int availableUnit ) {

        this.brandName = brandName;
        this.productType = productType;
        this.productName = productName;
        this.availableUnit = availableUnit;
    }


    public String getBrandName () {
        return brandName;
    }

    public void setBrandName ( String brandName ) {
        this.brandName = brandName;
    }

    public String getProductType () {
        return productType;
    }

    public void setProductType ( String productType ) {
        this.productType = productType;
    }

    public String getProductName () {
        return productName;
    }

    public void setProductName ( String productName ) {
        this.productName = productName;
    }

    public int getAvailableUnit () {
        return availableUnit;
    }

    public void setAvailableUnit ( int availableUnit ) {
        this.availableUnit = availableUnit;
    }

    public String getAccName () {
        return accName;
    }

    public void setAccName ( String accName ) {
        this.accName = accName;
    }

    public int getAccAvlb () {
        return accAvlb;
    }

    public void setAccAvlb ( int accAvlb ) {
        this.accAvlb = accAvlb;
    }

    public int getQuantityOrdered () {
        return quantityOrdered;
    }

    public void setQuantityOrdered ( int quantityOrdered ) {
        this.quantityOrdered = quantityOrdered;
    }
}
