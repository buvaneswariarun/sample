package com.company.vapasi.project.ShoppingCart;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Owner {
    static ArrayList <Map <String, Product>> universalCatalogue;
    private static ClothesCatalogueHandler clothesCatalogueHandler;

    public static int printMainMenu () {
        System.out.println ( "WELCOME TO MY CATALOGUE " );
        System.out.println ( "1. ADD A CLOTH TYPE" );
        System.out.println ( "2. ADD A ACCESSORY TYPE" );
        System.out.println ( "3. VIEW CATALOGUE" );
        System.out.println ( "ENTER YOUR CHOICE ::::" );
        Scanner input = new Scanner ( System.in );
        return (input.nextInt ());
    }

    public static void addClothType () {
        Scanner input = new Scanner ( System.in );
        System.out.println ( "ENTER BRAND NAME" );
        String brandName = input.nextLine ();
        Product clothProduct = new Product ( brandName );
        System.out.println ( "ENTER CLOTH TYPE" );
        String clothType = input.nextLine ();
        System.out.println ( "ENTER QUANTITY AVAILABLE" );
        int clothAvailableQuantity = input.nextInt ();
        clothProduct.setProductType ( "CLOTH" );
        clothProduct.setProductName ( clothType );
        clothProduct.setAvailableUnit ( clothAvailableQuantity );
        addProductToMainCatalogue ( brandName , clothProduct );
    }

    public static void addAccessory () {
        Scanner input = new Scanner ( System.in );
        System.out.println ( "ENTER BRAND NAME" );
        String brandName = input.nextLine ();
        Product accProduct = new Product ( brandName );
        System.out.println ( "ENTER ACCESSORY TYPE" );
        String accType = input.nextLine ();
        System.out.println ( "ENTER QUANTITY AVAILABLE" );
        int accAvailableQuantity = input.nextInt ();
        accProduct.setProductType ( "ACCESSORY" );
        accProduct.setProductName ( accType );
        accProduct.setAvailableUnit ( accAvailableQuantity );
        addProductToMainCatalogue ( brandName , accProduct );
    }

    public static void addProductToMainCatalogue ( String prodName , Product product ) {
        clothesCatalogueHandler.addProductToMainCatalogue ( prodName , product );
    }


    public static Map <String, Product> nameProd ( Product newProd ) {
        System.out.println ( newProd );
        Map <String, Product> prodMap = new HashMap <String, Product> ();
        System.out.println ( prodMap );
        prodMap.put ( newProd.getBrandName () , newProd );
        return prodMap;
    }

    public static void promptAgain () {
        Scanner promptInput = new Scanner ( System.in );
        System.out.println ( "Do you wish to continue ? Press YES/NO" );
        if (promptInput.nextLine ().equalsIgnoreCase ( "YES" )) {
            int alternate = printMainMenu ();
            handleUserInput ( alternate );
        } else {
           System.exit ( 0 );
        }

    }
public static void handleUserInput(int mainMenuInput ){
    switch (mainMenuInput) {
        case 1: {
            addClothType ();
            promptAgain ();
        }
        case 2:
            addAccessory ();
            promptAgain ();
        case 3:
            clothesCatalogueHandler.printCatalogue ( ClothesCatalogueHandler.universalCatalogue );
            promptAgain ();
    }
}
    public static void main ( String[] args ) {

        clothesCatalogueHandler = new ClothesCatalogueHandler ();
        clothesCatalogueHandler.populateData ();

        int mainMenuInput = printMainMenu ();
        handleUserInput(mainMenuInput);



    }
}
