package com.company.vapasi.project.ShoppingCart;

import java.util.ArrayList;
import java.util.Map;

public class Order {
    String customerName;

    public Order ( String customerName , ArrayList <Product> orderedItems ) {
        this.customerName = customerName;
        this.orderedItems = orderedItems;
    }

    ArrayList <Product> orderedItems;

    public String getCustomerName () {
        return customerName;
    }

    public void setCustomerName ( String customerName ) {
        this.customerName = customerName;
    }

    public ArrayList <Product> getOrderedItems () {
        return orderedItems;
    }

    public void setOrderedItems ( ArrayList <Product> orderedItems ) {
        this.orderedItems = orderedItems;
    }
}
