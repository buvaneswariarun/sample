package com.company;

import java.util.*;

public class Brand {
    String brandName;

    public String getBrandName() {
        return brandName;
    }

    public Map<String, Map<String, Integer>> getClothesMap() {
        return clothesMap;
    }

    public Map<String, Map<String, Integer>> getAccessMap() {
        return accessMap;
    }

    public Brand(String brandName, Map<String, Map<String, Integer>> clothesMap, Map<String, Map<String, Integer>> accessMap) {
        this.brandName = brandName;
        this.clothesMap = clothesMap;
        this.accessMap = accessMap;
    }

    Map<String, Map<String, Integer>> clothesMap = new HashMap<String, Map<String, Integer>>();
    Map<String, Map<String, Integer>> accessMap = new HashMap<String, Map<String, Integer>>();

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public void setClothesMap(Map<String, Map<String, Integer>> clothesMap) {
        this.clothesMap = clothesMap;
    }

    public void setAccessMap(Map<String, Map<String, Integer>> accessMap) {
        this.accessMap = accessMap;
    }
}
