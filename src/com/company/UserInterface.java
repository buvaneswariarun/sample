package com.company;

import com.company.vapasi.project.ShoppingCart.ClothesCatalogueHandler;
import com.company.vapasi.project.ShoppingCart.Product;

import java.util.ArrayList;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;

public class UserInterface {

    public static int showMainMenu(){
        System.out.println ( "Welcome to the shopping UI" );
        System.out.println ("1 --> View Catalogue");
        System.out.println ("2 --> Buy A Product");
        System.out.println ("3 --> View Shopping Cart");
        System.out.println ("4 --> Confirm and Exit");
        System.out.println ( "Enter your choice : " );
        Scanner userInput = new Scanner ( System.in );
        int input = userInput.nextInt ();
        return input;
    }
    public static void viewCatalogue(){

        ClothesCatalogueHandler.createCatalogue();


    }
    public static void printCatalogue(Map catalogue){
        ArrayList<String> brandList = new ArrayList <String> (  );
        ArrayList<Product> productArrayList = new ArrayList <Product> (  );

        if(catalogue != null){
            System.out.println ("**************************CATALOGUE*************************");
            for (Object entry : catalogue.entrySet ()) {
                if(entry instanceof Map.Entry){
                    brandList.add((((Map.Entry) entry).getKey ()).toString ());
                    productArrayList.add((Product)(((Map.Entry) entry).getValue ()));
                }
            };
            System.out.println (brandList.stream ().collect ( Collectors.toList () )) ;
            }
        }

    public static void main ( String[] args ) {
        ClothesCatalogueHandler clothesCatalogueHandler = new ClothesCatalogueHandler ();
        clothesCatalogueHandler.populateData ();

        int userChoice = showMainMenu();

        switch(userChoice){
            case 1: viewCatalogue();

        }
    }
}
