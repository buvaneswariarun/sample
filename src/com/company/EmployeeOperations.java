package com.company;

public class EmployeeOperations {

    public static void compareSalary(Employee emp1, Employee emp2){
        if(emp1.getSalary()>emp2.getSalary()){
            System.out.println(emp1.getEmployeeName()+" has more salary");
        }else{
            System.out.println(emp2.getEmployeeName()+" has more salary");
        }
    }
    public static void main(String[] args) {

        Employee emp1 = new Employee(1001,"Aadhya Rajiv",1234567);
        Employee emp2 = new Employee(1002,"Ananya",345678);
        Employee emp3 = new Employee(1003,"Saarthak Donthi",7453453);

        compareSalary(emp1,emp2);
    }
}
