package com.company;

import javax.swing.*;
import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamsExample {

    public static void main ( String[] args ) {
        String[] stringArray = {"USA", "Japan", "France", "Germany", "India", "U.K.","Canada"};
        Stream <String> sampStream = Stream.of ( "abc" , "" , "def" , "ghi" , "jk" );
        System.out.println (sampStream.filter ( forFilter ).collect ( Collectors.toList () ));
        List <Integer> integers = Arrays.asList ( 9, 10, 3, 4, 7, 3, 4 );
        System.out.println ( Arrays.stream ( stringArray ).map (toUpperCase  ).collect ( Collectors.toList () ));
        System.out.println (integers.stream ().distinct ().map ( distinctSquare ).collect ( Collectors.toList () ));
        List <Integer> integerCount = Arrays.asList ( 1,2,2,4,2,5);
        System.out.println (integerCount.stream ().filter (integer -> integer == 2 ).count ());
        List<Integer> list = Arrays.asList ( 2,4,1,3,7,5,9,6,8 );
        Optional<Integer> maxNumber = list.stream ().max ( Integer::compareTo );
        Optional<Integer> minNumber = list.stream ().min ( Integer::compareTo );
        System.out.println ("maxNumber = "+maxNumber);
        System.out.println ("minNumber = "+minNumber);
        //Stream<Date> stream = Stream.generate(() -> { return new Date(); });
        //stream.forEach(p -> System.out.println(p));
        Stream<String> stream = Stream.of("A B CDE FRG".split(" "));
        stream.forEach(p -> System.out.println(p));

        List<String> memberNames = new ArrayList <> ();
        memberNames.add("Amitabh");
        memberNames.add("Shekhar");
        memberNames.add("Aman");
        memberNames.add("Rahul");
        memberNames.add("Shahrukh");
        memberNames.add("Salman");
        memberNames.add("Yana");
        memberNames.add("Lokesh");
        System.out.println (memberNames.stream ().filter ( s -> s.startsWith ( "A" ) ).collect ( Collectors.toList () ));

        String[][] arr = {{"a","b"},{"g","h"}};

        Stream<String[]> s1 = Stream.of ( arr );
        System.out.println (s1.filter ( e -> e.equals ( "a" ) ).count ());
        Stream<String> s2 = s1.flatMap ( x -> Arrays.stream ( x ) );
        System.out.println (s2.filter (e-> e.equals ( "a" )  ).count () );

    }

    static Predicate <String> forFilter = new Predicate <String> () {
        @Override
        public boolean test ( String s ) {
            return (s.length ()> 2);
        }
    };
    static Function<String,String> toUpperCase = new Function<String,String>(){

        @Override
        public String apply ( String s ) {
            return s.toUpperCase ();
        }
    };
    static Function<Integer,Integer> distinctSquare = new Function <Integer, Integer> () {
        @Override
        public Integer apply ( Integer integer ) {
            return integer*integer;
        }
    };
}
