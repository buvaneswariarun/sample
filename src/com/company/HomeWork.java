package com.company;

import java.sql.SQLOutput;
import java.util.Scanner;

public class HomeWork {
    public static void factorial(int n) {

        int fact = 1;
        for (int i = 1; i <= n; i++) {
            fact = fact * i;
        }
        System.out.println(fact);
    }

    public static void fibonacciSeries(int n) {

        int n1, n2, n3;
        n1 = 0;
        n2 = 1;
        System.out.println(n1 + "\n" + n2);
        for (int i = 2; i < n; i++) {
            n3 = n1 + n2;
            n1 = n2;
            n2 = n3;
            System.out.println(n3);
        }
    }

    public static void palindrome(long number) {
        long originalNumber = number;
        long reversedNumber = 0;
        long remainder = 0;
        for (; number != 0; number /= 10) {
            remainder = number % 10;
            reversedNumber = reversedNumber * 10 + remainder;
        }
        if (originalNumber == reversedNumber) {
            System.out.println("Palindrome");
        } else {
            System.out.println("Not a palindrome");
        }
    }

    public static void primeNumberGenerator(int input) {
        System.out.println(1);
        System.out.println(2);
        for (int j=3;j<=input;j++) {
            int counter=0;
            for (int i = 2; (i <=(j/2)); ++i) {
                if ((j % i) == 0) {
                    counter++;
                }
            }if(counter<1){
                System.out.println(j);
            }
        }

    }
    public static void matrixRepresentation(int[][] multiarr){
    for(int i = 0;i<multiarr.length;++i){
        for(int j = 0;j<multiarr[i].length;++j){
            System.out.print(multiarr[i][j]+" ");
        }
        System.out.println();
    }
    }
    public static int[][] inputMatrix(){
        int rows;
        int cols;
        System.out.println("Enter the rows and columns of matrix");
        Scanner in = new Scanner(System.in);
        rows = in.nextInt();
        cols = in.nextInt();
        System.out.println(rows+"##"+cols);
        System.out.println("Enter content");
        int[][] result = new int[rows][cols];

        for(int i = 0;i<cols;i++){
            for(int j =0;j<rows;j++){
                result[i][j] = in.nextInt();
            }
        }

        matrixRepresentation(result);
        return result;
    }
    public static void matrixAddition(){
        int [][] input1 = inputMatrix();
        int [][] input2 = inputMatrix();

        int[][] sum_matrix=new int[input1.length][input2.length];

        for(int i = 0;i<input1.length;i++){
            int sum=0;
            for(int j = 0;j<input2[i].length;j++){
                sum_matrix[i][j]=input1[i][j]+input2[i][j];
            }

        }
        matrixRepresentation(sum_matrix);
    }


    public static void main(String[] args) {
        factorial(5);
        fibonacciSeries(10);
        palindrome(11111111);
        primeNumberGenerator(20);
        int[][] multiarr = {
            {1,2,3},{4,5,6},{7,8,9}
        };
        matrixRepresentation(multiarr);
        matrixAddition();

    }
}
